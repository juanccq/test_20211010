<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ApiTest extends TestCase
{
    /**
     * A basic feature test if the route is working.
     *
     * @return void
     */
    public function test_it_should_be_work()
    {
        $numberToTest = 21;
        $response = $this->get('/api/main/' . $numberToTest);

        $response->assertStatus(200);
    }

    /**
     * A basic feature test if the index returned is divisible by '3' and returns 'Big'
     *
     * @return void
     */
    public function test_it_should_be_big_responses()
    {
        $numberToTest = 21;
        $response = $this->get('/api/main/' . $numberToTest);
        $response->dump();

        $this->assertTrue($response[2] === 'Big' && $response[17] === 'Big');
    }

    /**
     * A basic feature test if the index returned is divisible by '5' and returns 'Bang'
     *
     * @return void
     */
    public function test_it_should_be_bang_responses()
    {
        $numberToTest = 21;
        $response = $this->get('/api/main/' . $numberToTest);
        $response->dump();

        $this->assertTrue($response[4] === 'Bang' && $response[9] === 'Bang');
    }

    /**
     * A basic feature test if the index returned is divisible by '7' and returns 'Theory'
     *
     * @return void
     */
    public function test_it_should_be_theory_responses()
    {
        $numberToTest = 21;
        $response = $this->get('/api/main/' . $numberToTest);
        $response->dump();

        $this->assertTrue($response[6] === 'Theory' && $response[13] === 'Theory');
    }


    /**
     * A basic feature test if the index returned is divisible by '3' and '5' and returns 'BigBang'
     *
     * @return void
     */
    public function test_it_should_be_bigbang_responses()
    {
        $numberToTest = 21;
        $response = $this->get('/api/main/' . $numberToTest);
        $response->dump();

        $this->assertTrue($response[14] === 'BigBang');
    }

    /**
     * A basic feature test if the index returned is divisible by '3' and '7' and returns 'BigBang'
     *
     * @return void
     */
    public function test_it_should_be_bigtheory_responses()
    {
        $numberToTest = 21;
        $response = $this->get('/api/main/' . $numberToTest);
        $response->dump();

        $this->assertTrue($response[20] === 'BigTheory');
    }


    /**
     * A basic feature test if the index returned is divisible by '3', '5' and '7' and returns 'BigBangTheory'
     *
     * @return void
     */
    public function test_it_should_be_bigbangtheory_responses()
    {
        $numberToTest = 105;
        $response = $this->get('/api/main/' . $numberToTest);
        $response->dump();

        $this->assertTrue($response[104] === 'BigBangTheory');
    }
}
