<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});


Route::get('main/{num}', function ($num) {
    $response = [];

    for ($i = 1; $i <= $num; $i++) {
        if ($i % 3 == 0 && $i % 5 == 0 && $i % 7 == 0) {
            $response[] = 'BigBangTheory';
        } elseif ($i % 3 == 0 && $i % 5 == 0) {
            $response[] = 'BigBang';
        } elseif ($i % 3 == 0 && $i % 7 == 0) {
            $response[] = 'BigTheory';
        } elseif ($i % 5 == 0 && $i % 7 == 0) {
            $response[] = 'BangTheory';
        } elseif ($i % 3 == 0) {
            $response[] = 'Big';
        } elseif ($i % 5 == 0) {
            $response[] = 'Bang';
        } elseif ($i % 7 == 0) {
            $response[] = 'Theory';
        } else {
            $response[] = $i;
        }
    }

    return $response;
});
